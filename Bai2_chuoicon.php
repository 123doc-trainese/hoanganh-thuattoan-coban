<?php

function SubString($str)
{
    $sub_arr = str_split($str); //tach mang

    $max_string = '';   //chuoi con dai nhat
    $max_length = 0;    
    for ($i = 0; $i < count($sub_arr); $i++) {
        $letter = $sub_arr[$i];
        if (strpos($max_string, $letter) === false) {
            $max_string .= $letter;
            $max_length = max($max_length, strlen($max_string));
        } else {
            $max_string = substr($max_string, strpos($max_string, $letter) + 1);
            $max_string .= $letter;
        }
    }
    return $max_length;
}

$string = "pwwkew";
echo SubString($string);
