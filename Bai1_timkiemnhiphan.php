<?php

function num_first($array, $value)
{
    $position = -1;
    //kiểm tra mảng  
    if (count($array) == 0) {
        return $position;
    }
    $left = 0; //vi tri đầu tiên
    $right = count($array) - 1; //vi tri cuối cùng
    while ($left <= $right) {
        $mid = floor(($left + $right) / 2);

        if ($array[$mid] < $value) {
            $left = $mid + 1; //// giá trị tìm kiếm bên phai
        } elseif ($array[$mid] > $value) {
            $right = $mid - 1; // giá trị tìm kiếm nằm bên trái
        } else {
            $position = $mid;
            $right = $mid - 1;  //tro key lay gia tri ben trai
        }
    }
    return $position;
}

function num_last($array, $value)
{
    $position = -1;
    //kiểm tra mảng  
    if (count($array) == 0) {
        return $position;
    }

    $left = 0; //vi tri đầu tiên
    $right = count($array) - 1; //vi tri cuối 
    while ($left <= $right) {
        $mid = floor(($left + $right) / 2);

        if ($array[$mid] < $value) {
            $left = $mid + 1;
        } elseif ($array[$mid] > $value) {
            $right = $mid - 1;
        } else {
            $position = $mid;
            $left = $mid + 1;  ///lay gia tri ben phai
        }
    }
    return $position;
}

$arr = array(1, 2, 4, 4, 4, 4, 4, 4, 4, 9);
$val  = 4;

$first = num_first($arr, $val);
$last = num_last($arr, $val);

///hien thi
if ($first == -1 || $last == -1) {
    echo "-1 -1";
} else {
    echo $first . ' ' . $last;
}
